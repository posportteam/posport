<script type="text/javascript" src="/libs_js/site_comments.js"></script>

<!-- Список комментариев -->
<h3>{LT_COMMENTS}</h3>

<? if($_ENV['id_user']){ ?>

<div class='comment_add'>
	<div class='comment_author'>
		<a href='/user{AUTHOR}'>
		<div class='avatar_50' style='background: url("{AVATAR}") no-repeat center; 
background-size: cover;	-webkit-background-size: cover;	-0-background-size: cover;	-moz-background-size: cover;'>
			&nbsp;
		</div>
		</a>
	</div>
	<div class='comment_info'>
		<textarea id='comment_add_text' class='autoresize' wrap='virtual' placeholder=''></textarea>
		<div id='comment_add_but'>
			<button id='new_comment_add'>{LT_COMMENT_GO}</button>
			<button id='new_comment_clear'>{LT_CLEAR}</button>
		</div>
	</div>
	
</div>

<? };?>

<div class='comments'  id='comments'>
	{COMMENTS}
</div>

