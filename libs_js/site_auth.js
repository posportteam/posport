/////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////
///////////////////// РЕГИСТРАЦИЯ  //////////////////////////////
/////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////

$(document).ready(function(){
	
	
	//$('#but_license').live('click',function(e){
	//	$('#license').hide();
	//	$('#regForm').show();
	//});
	//$('#but_reg_sms, #but_reg').prop('disabled',true);

	// Подсказки на фокусе инпута
	//$('#regForm input').live('focus',function(e){
	//	$('span[for='+$(this).attr('id')+']').show();
	//});
	
	// Выбрана синхронизация
	/* $('div.init_user').live('click',function(e){
		
		if($(this).hasClass('init_user_cur')){
			$(this).removeClass('init_user_cur');
			$('#reg_id_user').val('');
			$('#div_dop_info input, #div_dop_info select, #div_osn_info input').removeAttr('disabled');
		}else{
			$('div.init_user').removeClass('init_user_cur');
			$(this).addClass('init_user_cur');
		
			var id_user = parseInt($(this).attr('id_user'));
			var adress_id = parseInt($(this).find('input.adress_id').val());
			var adress_index = parseInt($(this).find('.adress_index').val());
			var adress_title = $(this).find('.adress_title').val();
			var adress_house = $(this).find('.adress_house').val();
			var adress_flat = parseInt($(this).find('.adress_flat').val());
			var id_city = parseInt($(this).find('.id_city').val());
			var title = $(this).find('.city_title').text();
			var obl_title = $(this).find('.obl_title').text();
			var sex = $(this).find('.sex').val();
			var born = $(this).find('.born').val();

			if(id_user>0){
				$('#reg_id_user').val(id_user);
				if(adress_index>0){$('#reg_adress_index').val(adress_index).attr('disabled','disabled');};
				if(adress_id>0){$('#reg_adress_id').val(adress_id).attr('disabled','disabled');};
				if(adress_title){$('#reg_adress_title').val(adress_title).attr('disabled','disabled');};
				if(adress_house){$('#reg_adress_house').val(adress_house).attr('disabled','disabled');};
				if(adress_flat){$('#reg_adress_flat').val(adress_flat).attr('disabled','disabled');};
				if(sex){$('#reg_sex').val(sex).attr('disabled','disabled');};
				if(born){$('#reg_born').val(born).attr('disabled','disabled');};
				if(id_city){
					$('#reg_city').tokenInput("clear");
					$('#reg_city').tokenInput("add",{'id': id_city, 'title':  title, 'obl_title': obl_title});
				};
				get_geocode_forobj($('.div_adress'));
			};
		};
		
		if(check_not_null_field($('#regForm'),true)){
			$('#but_reg_sms').removeAttr('disabled');
		}else{
			$('#but_reg_sms').attr('disabled','disabled');
		}
		
	});
	 */

	
	// Запрос на получение геокода с 2ГИС
	//$('.div_adress input').live('change',function(e){
	//	get_geocode_forobj($('.div_adress'));
	//});
	
	
	// Проверка правильности заполненных поле
/* 	$('#regForm input, #regForm select').live('change',function(e){

		if(check_not_null_field($('#regForm'),false)){
			$('#but_reg_sms').removeAttr('disabled');
			alert('true');
		}else{
			$('#but_reg_sms').attr('disabled','disabled');
		}
		
		
		var id = $(this).attr('id');
		var reg = /\w/g;
		var error = false;
		var error_tel = false;
		
		if(id=='reg_telephone'){	// При снятии фокуса проверяем телефон на корректность и занятость
			var telephone = reform_mobile_phone($(this).val().trim());
			if(telephone){
				$(this).val(telephone);
				if(check_tel(telephone)){
					error_tel = true;// Телефон занят. Выводим ошибку
				};
			}else{
				error = true;// Ошибка ввода, не верный телефон
			};
			
			// Подстветка если телефон занят
			if(error_tel){
				$('#err_telephone').show();
			}else{
				$('#err_telephone').hide();
			};
			
		}else if(id=='reg_name'){ // Проверяем фамилию
			var name = $(this).val().trim();
			if(reg.test(name) || name==''){	error = true;	};

		}else if(id=='reg_second_name'){ // Проверяем Имя пользователя
			var second_name = $(this).val().trim();
			if(reg.test(second_name) || second_name==''){	error = true;};
		};

		// Если есть ошибка, сообщаем
		if(error){
			$(this).val('');
			$('span[for='+id+']').show();
			$(this).css('border','1px solid red');
			
		}else{
			$(this).css('border','');
			$('span[for='+id+']').hide();
		};	
	}); */
	
	
	// Возможные синхронизации
/* 	$('#reg_telephone, #reg_name, #reg_second_name').live('change',function(e){
		var telephone = $('#reg_telephone').val().trim();
		var name = $('#reg_name').val().trim();
		var second_name = $('#reg_second_name').val().trim();
		
		$('#div_dop_info input, #div_dop_info select, #div_osn_info input').removeAttr('disabled');
		
		$('#div_reg_init div.init_user').detach();
		$('#div_reg_init').hide();
		$('#reg_id_user').val('');
		$('#reg_adress_index').val('');
		$('#reg_adress_id').val('');
		$('#reg_adress_title').val('');
		$('#reg_adress_house').val('');
		$('#reg_adress_flat').val('');
		$('#reg_born').val('');
		$('#reg_sex').val(0);

		if(telephone && name && second_name && !check_tel(telephone)){
			

			$.ajax({
				type: "POST",	url: "ajax_auth.php", cache: false,
				data: { action: 'reg',  name: name, second_name: second_name, telephone: telephone},
				beforeSend: function(){
					$('#img_ajax_name').show();
				},
				success: function(response){
					$('#img_ajax_name').hide();
					if(response!='null'){
						var obj = JSON.parse(response);
						var response_text = "";
						$.each(obj, function (index, user) {
							var club='';
							if(user.groups.length==1){
								club = 'Клуб: \"' + user.groups[0].club_title+'\", ' + user.groups[0].section_title + ', группа: ' + user.groups[0].group_title;
							}else if(user.groups.length){
								club = 'Секции: ';
								var i = 0;
								$.each(user.groups, function (group_index, group) {
									if(i){club+=', ';}; 
									i++;
									club+=group.club_title + '(' + group.section_title + ')';								
								});
							};
							if(user.status){var status=', ' + user.status;};
							response_text+="<div class='init_user' id_user="+user.id+">";
							response_text+=user_avatar_thumb(user.id);
							response_text+="<div class='user_name'><a href='/?page=user&id="+user.id+"'>"+ user.name + " " + user.second_name +"</a></div>";
							response_text+="<div> Город: <span class='city_title'>"+ user.contacts.city + "</span>, <span class='obl_title'>"+user.contacts.obl+"</span></br>";
							response_text+=club + status +"</div>";
							response_text+="<input type='hidden' class='id_city' value='"+user.contacts.id_city+"'>";
							response_text+="<input type='hidden' class='sex' value='"+user.sex+"'>";
							response_text+="<input type='hidden' class='born' value='"+user.born+"'>";
							response_text+="<input type='hidden' class='adress_id' value='"+user.contacts.adress_id+"'>";
							response_text+="<input type='hidden' class='adress_index' value='"+user.contacts.index+"'>";
							response_text+="<input type='hidden' class='adress_title' value='"+user.contacts.title+"'>";
							response_text+="<input type='hidden' class='adress_house' value='"+user.contacts.house+"'>";
							response_text+="<input type='hidden' class='adress_flat' value='"+user.contacts.flat+"'>";
							response_text+="</div>";

						});
						$('#div_reg_init div.init_user').detach();
						$('#div_reg_init').append(response_text).show();
					};
				}
			});
		};
	});
	 */
	
	/* $('#but_reg_sms').live('click',function(e){
		if(check_not_null_field($('#regForm'),true)){
			var tel = $('#reg_phone').val();
			// $.ajax({  
				// type: "GET",  
				// url: "ajax/ajax_send_mess.php?action=reg",  
				// data: {telephone: tel},	
				// beforeSend: function(){
					// $('#regForm input, #regForm select').attr('disabled','disabled');
					// $('#but_reg_sms').html('Отправляется SMS').attr('disabled','disabled');
				// },
				// success: function(response){ 
					// var obj = JSON.parse(response);
					// if(!obj.error){
						// $('#reg_sms').removeAttr('disabled');
					// };
				// }  
			// }); 
		}else{
			$('#but_reg_sms').attr('disabled','disabled');
		};
		return false;
	}); */
	
	

	
	
	

	
/////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////
///////////////////// ВХОД НА САЙТ //////////////////////////////
/////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////
		
		
	$('#auth').ajaxForm({ 
        target:        '#auth_loading',   // target element(s) to be updated with server response 
        beforeSubmit:  showRequest,  // pre-submit callback 
        success:       showResponse,  // post-submit callback 
		//dataType:  json        // 'xml', 'script', or 'json' (expected server response type) 
        
		// other available options: 
        //url:       url         // override for form's 'action' attribute 
        //type:      type        // 'get' or 'post', override for form's 'method' attribute 
        //clearForm: true        // clear all form fields after successful submit 
        //resetForm: true        // reset the form after successful submit 
        //timeout:   3000 
    });	
					
	function showRequest(formData, jqForm, options) { 
		$('#auth_loading').show();
	} 
	 
	function showResponse(response, statusText, xhr, $form)  { 
		var obj = JSON.parse(response);
		$('#auth_loading').html(obj.response_text);
					
		if(!obj.error){
			location.href=document.location.href;
		};
	}	
		
						
	$('body').on('click','#but_auth_users',function(e){
		$("#toolbar_settings").toggle();
		$("#auth_phone").focus();
	});
	

	
});