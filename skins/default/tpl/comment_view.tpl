<div class='comment {CLASS_COMMENT_VISIBLE}' data-id-comment='{COMMENT_ID}'>
	<div class='comment_author'>
		<a href='/user{AUTHOR}'>
		<div class='avatar_50' style='background: url("{AVATAR}") no-repeat center; 
background-size: cover;	-webkit-background-size: cover;	-0-background-size: cover;	-moz-background-size: cover;'>
			&nbsp;
		</div>
		</a>
	</div>
	<div class='comment_info' style='display:{DISPLAY_INFO};'>
		<div class='comment_author_name'><a href='/user{AUTHOR}'>{AUTHOR_NAME} <second_name>{AUTHOR_SECOND_NAME}</second_name></a></div>
		<div class='comment_text'>{COMMENT_TEXT}</div>
		<div class='comment_panel'><comment_date>{DATE_LT}: {DATE}</comment_date> {COMMENT_BUT_EDIT}</div>
	</div>
	<div class='comment_rec' style='display:{DISPLAY_REC};'>{LT_COMMENT_DELETING}. <a class='comment_del' data-event='commentRec'>{LT_RECOVERY_GO}!</a></div>
	
	<div class='loading'></div>
</div>