<div class='careers_sport_edit {NEW}'>
	<input type='text' class='careers_city city' placeholder='{LT_CITY}' value='{CITY}' for='user_careers_sport_city_{NUM_CAREERS}'>
	<input type='hidden' name='careers[sport][city][]' value='{ID_CITY}' id='user_careers_sport_city_{NUM_CAREERS}'>
	
	<input type='text' class='careers_sport sport' placeholder='{LT_SPORT}' value='{SPORT}' for='user_careers_sport_sport_{NUM_CAREERS}'>
	<input type='hidden' name='careers[sport][sport][]' value='{ID_SPORT}' id='user_careers_sport_sport_{NUM_CAREERS}'>
	
	
	
	<select class='careers_select club linklist' name='careers[sport][club][]' data-linklist-name='club' data-defval='{CLUB}' disabled='disabled'><option>{LT_CLUB}</option></select>
	<select class='careers_select trener linklist' name='careers[sport][trener][]' data-linklist-name='trener' data-defval='{TRENER}' disabled='disabled'><option>{LT_TRENER}</option></select>

	<input type='text' class='careers_date datepicker_from' value='{DATE_IN}' name='careers[sport][date_in][]' >
	<input type='text' class='careers_date datepicker_to' value='{DATE_OUT}' name='careers[sport][date_out][]' >
	
	<button class='but_save_small save_career_sport'>{LT_SAVE}</button>
</div>