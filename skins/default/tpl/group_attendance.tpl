<h3>{LT_ATTENDANCE}</h3>

<div id='select_month_attendance'>
{SELECT_MONTH} {SELECT_YEAR}
<button id='butShowAttendance' data-id-group='{ID_GROUP}'>{LT_SHOW}</button>
</div>

<table class = 'group_attendance' data-id-group='{ID_GROUP}' data-year='{YEAR}' data-month='{MONTH}'>
	<thead>
		{TH_TRAINING}
	</thead>
	<tbody>
		{TR_USER}
	</tbody>
</table>