<?php
class Controller_index extends Controller{
	function __construct(){
		$this->model = new Model_index();
		$this->view = new View_page();
	}
	
	function action_index()	{
		$data = $this->model->get_data();		
		$this->view->generate_page('index_content', $data);
	}
}
?>