///////////////////////////////////////////
///////////////////////////////////////////
//////// КОММЕНТАРИИ //////////////////////
///////////////////////////////////////////

$(document).ready(function(){
// Ответить
$('a.comment_otv').live('click',function(e){
	var name = $(this).parents('div.comment_info').find('second_name').text();
	$('#comment_add_text').focus().val(name+', ');	
	return false;
});

// Очистить кнопка
$('#new_comment_clear').live('click',function(e){
	$('#comment_add_text').focus().val('');
});

// Показывание кнопок добавления
$('#comment_add_text').live({
	focus: function(e){
		$('#comment_add_but').show(300);
	},
	blur: function(e){
		if(!$(this).val()){
			$('#comment_add_but').hide(300);
		};
	}
});


$('a.comment_edit').live('click', function(e){
	var div_comment = $(this).parents('div.comment');
	var id = div_comment.attr('data-id-comment');
	var div_text = div_comment.find('div.comment_text');
	var text = div_text.html().replace(/<br>/g,'\n').replace(/<br>/g,'\r').replace(/<br>/g,'\r\n').trim();
	div_text.html("<textarea id='comment_edit'></textarea><div style='display: none;' id='comment_edit_text'></div>");
	$('#comment_edit').autoResize().focus().html(text);
	$('#comment_edit_text').html(text);
});
	
$('#comment_edit').live('blur', function(e){
	var text_new = $(this).val().replace(/\n/g,'<br>').replace(/\r/g,'<br>').replace(/\r\n/g,'<br>').trim();
	var text_old = $('#comment_edit_text').html();
	var div_comment = $(this).parents('div.comment');
	var id = div_comment.attr('data-id-comment');
		if(text_new!=''){
			$.ajax({
				type: "POST",
				url: "ajax/ajax_comments.php?action=commentUpdate",
				data: {id: id, text: text_new},
				cache: false,
				beforeSend: function(){
					div_comment.find('div.loading').show();
				},
				success: function(response){ 
					div_comment.find('div.loading').hide();
					div_comment.find('comment_date').html('Редактировано ' + response);
					div_comment.find('div.comment_text').html(text_new);
				}
			});
		}else{
			$(this).parents('div.comment_text').html(text_old);
		};
});


// Удаление и восстановление статей



$("a.comment_del").live('click',function(e){
	var id = $(this).parents('div.comment').attr('data-id-comment');
	var event = $(this).attr('data-event');
	var div_comment = $('div.comment[data-id-comment='+id+']');
	$.ajax({
		type: "POST",
		url: "ajax/ajax_comments.php?action=" + event,
		data: {id: id},
		cache: false,
		beforeSend: function(){
			div_comment.find('div.loading').show();
		},
		success: function(response){
			if(event=='commentDel'){
				div_comment.find('div.comment_info').hide();
				div_comment.toggleClass('deleting');
				div_comment.find('div.comment_rec').show(300);
			}else if(event=='commentRec'){
				div_comment.find('div.comment_rec').hide();
				div_comment.toggleClass('deleting');
				div_comment.find('div.comment_info').show(300);
			};
			div_comment.find('div.loading').hide();
		}
    });
	return false;
});

// Добавить новый комментарий
$("#new_comment_add").live('click',function(e){
	var text = $('#comment_add_text').val().trim().replace(/\n/g,'<br>').replace(/\r/g,'<br>').replace(/\r\n/g,'<br>');
	var id_art = $('#comments_in_art').attr('data-id-art');
	var id_photo = 0;
	
	if(text && id_art){
		$.ajax({
			type: "POST",
			url: "ajax/ajax_comments.php?action=commentAdd",
			data: {text: text, id_art: id_art, id_photo: id_photo},
			cache: false,
			beforeSend: function(){
				$('#comments').html("<div class='loading' style='display: block;'></div>");
			},
			success: function(response){ 
				$('#comment_add_text').val('');
				$('#comments').html(response);
			}
		});
	}else{
		alert('Комментарий пуст');
	};
});

});