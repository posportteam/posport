<?php
class Controller_footer{
	function __construct(){
		$this->model = new Model_footer();	
		$this->tpl = new Tpl();	
	}

	function action(){
		$data = $this->model->get_data();
		$page = $this->tpl->generate('footer', $data);
		return $page;
	}
}

