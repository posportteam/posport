<div data-id-art='{ART_ID}' class='article_preview {CLASS_ART_VISIBLE}'>
	
	<div class='art_author'>
		<a href='/user{AUTHOR}'>
		<div class='avatar_50' style='background: url("{AVATAR}") no-repeat center; background-size: cover;	-webkit-background-size: cover;	-0-background-size: cover;	-moz-background-size: cover;'>
			&nbsp;
		</div>
		</a>
	</div>
	<div class='art_info'  style='display:{DISPLAY_INFO};'>
		<div class='art_author_name'><a href='/user{AUTHOR}'>{AUTHOR_NAME}</a></div>
		<div class='art_title'>{ART_TITLE}</div>
		<div class='art_text' href=''> {ART_TEXT}</div>
		<div class='art_dop_info'>
			<div class='art_date'>{DATE} {BUTTONSET_EDIT_ART}</div>
			<div class='art_comment'><a href='/article{ART_ID}#art_comments'>{LT_COMMENT_GO} {COMMENT_NUM}</a></div>
		</div>
	</div>
	<div class='art_rec' style='display:{DISPLAY_REC};'>{LT_ART_DELETING}. <a class='artRec' data-event='artRec'>{LT_RECOVERY_GO}!</a></div>
</div>		
