<!--- START DIALOGS -->
		<div id='dialog-add-city'  title='Добавить новый город' class='dialog' hidden>
			<p><label>Федеральный округ: </label><?php echo $select_fedok?></p>
			<p><label>Область: </label><?php echo $select_ob?></p>
			<p><label>Название города: </label><input type='text' name='title_city'></p>
			<p><label>Численность: </label><input type='number' min='1000' name='chisl'></p>
		</div>
		
		<div id='dialog-edit-zal'  title='Редактировать / добавить зал' class='dialog' hidden>
			<p><label>Полное название: </label><input type='text' name='title'></p>
			<p><label>Краткое название: </label><input type='text' name='kr_title'></p>
			<p><label>Федеральный округ: </label><?php echo $select_fedok?></p>
			<p><label>Область: </label><?php echo $select_obl?></p>
			<p><label>Город: </label><?php echo $select_city?></p>
			<p><label>Адрес: </label><input type='text' name='adress'></p>
			<p><label>Конт. телефон: </label><input type='text' name='telephone'></p>
		</div>
		
		<div id='dialog-del-zal'  title='Удалить зал зал' class='dialog' hidden>
			<p><span class='ui-icon ui-icon-alert' style='float:left; margin:0 7px 20px 0;'></span>
			Вы уверены, что хотите удалить зал из базы данных?! Возможно, к залу привязаны соревнования и секции!</p>
		</div>
		
		<div id='dialog-save-sports' title='Сохранение в правилах!' hidden>
					<p><span class='ui-icon ui-icon-alert' style='float:left; margin:0 7px 20px 0;'></span>
					Сохранение происходить по всем возрастам среди мужчин и женщин для спорта <span id='text_id_sports'></span>. Проверьте данные ещё раз!</p>
		</div>
		


<!--- END DIALOGS -->