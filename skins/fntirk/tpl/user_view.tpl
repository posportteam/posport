	<div id='user_control'>
		<img src='/{USER_PHOTO_SRC}' class='photo' id='user_avatar'>
		{USER_BUTTON_MY}
		{USER_BUTTON_OTHER}
		{USER_BUTTON_ADMIN}
		{USER_BUTTON_TRENER}
	</div>
	
	<div id='user_info'>
		<h4>{USER_NAME}</h4>
		
		<div>
			{DIV_INFO}
		</div>
		<div>
			{DIV_ZACHETKA}
		</div>
		<div>
			{DIV_RANGS}
		</div>
		<div>
			{DIV_CAREERS}
		</div>
		<div>
			{DIV_CONTACTS}
		</div>

		
		{USER_BOIGRAPHY}
		{USER_BLOG}
	</div>