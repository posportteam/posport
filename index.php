<?php
ob_start(); 
	// Устанавливаем кодировку и уровень ошибок 
	header("Content-Type: text/html; charset=utf-8");  
	error_reporting(E_ALL);  
	error_reporting(0);  
	ob_end_clean();
	// система MVC
	require_once './libs_php/core.php'; // подключаем файлы ядра - класс модулей 
	
	// Маршрутизатор
	require_once './libs_php/route.php'; // подключаем маршрутизатора, который подгружем контроллеры и модели
	
	//require_once './debug.php'; // Дебаггер  
    require_once './config.php';  // Подключаем конфигурационный файл
	require_once './libs_php/db_connect.php'; // Подключаем к базе данных
	require_once './libs_php/classes.php'; // Подключаем файл классов  
	require_once './libs_php/functions.php'; // Подключаем файл общих функций 
	require_once './libs_php/user_connect.php'; // Подключаем определние пользователя и его прав 
	require_once './libs_php/set_skin.php'; // Подключаем определние и установки скина
	
	

	/*
	Здесь обычно подключаются дополнительные модули, реализующие различный функционал:
		> аутентификацию
		> кеширование
		> работу с формами
		> абстракции для доступа к данным
		> ORM
		> Unit тестирование
		> Benchmarking
		> Работу с изображениями
		> Backup
		> и др.
	*/

	Route::start(); // запускаем маршрутизатор
?>