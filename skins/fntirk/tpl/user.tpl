<script type="text/javascript" src="/libs_js/site_user.js"></script>
<!-- <script type="text/javascript" src="/libs_js/jquery.Jcrop.min.js"></script> -->
<script type="text/javascript" src="/libs_js/jquery.Jcrop.js"></script>

<link rel="stylesheet" type="text/css" media="screen" href="/skins/fntirk/css/user.css"/>
<link rel="stylesheet" type="text/css" media="screen" href="/css/jquery.Jcrop.css"/>
<div id='user_profile'>
	
	{USER_PAGE}
	
</div>		

<div id='avatar_change'>
	<form id='avatar_change_form' action='/ajax/ajax_upload.php?action=uploadAvatar' method='POST' enctype='multipart/form-data'>
		<div><input type='file' id='avatar_change_file' name='avatar_change_file' accept='image/jpeg, image/pjpeg'></div>
		<input type='submit' id='avatar_change_but_download' value='Загрузить'>
	</form>
	
	<div id='avatar_tmp_preview'>
		<div id='avatar_tmp_preview_div'></div>
		<div class='loading'></div>
		<input type='hidden' name='avatar_tmp_preview_img_x' id='avatar_tmp_preview_img_x'>
		<input type='hidden' name='avatar_tmp_preview_img_y' id='avatar_tmp_preview_img_y'>
		<input type='hidden' name='avatar_tmp_preview_img_w' id='avatar_tmp_preview_img_w'>
		<input type='hidden' name='avatar_tmp_preview_img_h' id='avatar_tmp_preview_img_h'>
	</div>
</div>
