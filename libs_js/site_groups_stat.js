$(document).ready(function(){

	Highcharts.setOptions({
			lang: {
				shortMonths: ['Янв', 'Фев', 'Мар', 'Апр', 'Май', 'Июн', 'Июл', 'Авг', 'Сен', 'Окт', 'Ноя', 'Дек'],
				months: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
				weekdays: ['Воскресение', 'Понедельник', 'Вторник', 'Среда', 'Четверег', 'Пятница', 'Суббота'],
				downloadJPEG: 'Скачать в JPEG',
				downloadPNG: 'Скачать в PNG',
				downloadSVG: 'Скачать в SVG',
				downloadPDF: 'Скачать в PDF',
				printButtonTitle: 'Печать',
				loading: 'Загрузка',
				exportButtonTitle: 'Сохранить график как...',
			}
		});

		var data = {id_trener:1};
		var seriesOptions = [];
		
		$.ajax({  
			type: "POST",  
			url: "/ajax/ajax_groups.php?action=getGroupsStatAttendance",  
			data: data,
			beforeSend: function(){
				$('#chartLoading').show();
			},
			success: function(response){
				var obj = JSON.parse(response);
				var i = 0;
				$.each(obj, function(key, val){
					seriesOptions[i] = {
						name: obj[key].title,
						data: obj[key].attendance,
						tooltip: {
							valueDecimals: 0
						}
					};			
					i++;		
				});
				$('#chartLoading').hide();
				if(seriesOptions.length){
					createChart(seriesOptions);
				};
			}

		});

	
	function createChart(seriesOptions) {
		chart = new Highcharts.StockChart({
		    chart: {
		        renderTo: 'containerStatGroup'
		    },
		    rangeSelector: {
				buttons: [
					{type: 'month', count: 1, text: '1м'},
					{type: 'month', count: 3, text: '3м'},
					{type: 'month', count: 6, text: '6м'},
					{type: 'year', count: 1, text: '1г'},
					{type: 'all', text: 'Всё'}
				],
		        selected: 0
		    },
		    tooltip: {
				xDateFormat: '%d.%m.%Y',
		    	pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.y} человек</b><br/>',
		    },
		    // legend: {
				// enabled: true,
				// layout: 'vertical',
				// align: 'right',
				// verticalAlign: 'top',			
				// x: -10,
				// y: 60,
				// itemStyle: {padding: '10px'}
			// },
		    series: seriesOptions
		});
	};
	
});