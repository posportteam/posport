$(document).ready(function(){

	
	var cache_days = new Array();
	var cache_prop = new Array();

	// Установим текущую дату
		
	var today = new Date();
	var curr_year = today.getFullYear();
	var curr_month = twoDigits(today.getMonth()+1);
	var curr_day = twoDigits(today.getDay()+1);
	var curr_date = today.getFullYear()+'-'+twoDigits(today.getMonth()+1);
	var is_first = true;
	
	
	// Функция возвращает число в виде строки из двух символов
	function twoDigits(str)	{ 
		if (typeof(str) != 'string') {
			str = str.toString();
		}
		return str.length < 2 ? '0' + str : str;
	};
	
		function loadMonthCalendar(year, month)	{ // Вызываем аяксом скрипт get-calendar, возвращающий нам в виде JSON список дней
		var days = new Array();
		
    	$.post(
			'/ajax/ajax_site_cal.php',
       		{year: year, month: month},
       		function (data){
				if(data!=null){

					// Сохраняем полученные значения в кэш
					$.each(data, function (index,json_days) {
						var day = twoDigits(json_days.day);
						cache_prop[year+'-'+month+'-'+day] = [];

						// Массив дат запрошенного месяца
						days.push(day);
						
						// Кеш для хранения дополнительной информации (название и тип) для всплывающих подсказок
						for (var key in json_days.events) {
							var obj_prop = {"title":json_days.events[key].title, "city":json_days.events[key].city, "sport":json_days.events[key].sport};
							cache_prop[year+'-'+month+'-'+day].push(obj_prop);
						};

					});
				};
				
				//Кеш для хранения дат
				cache_days[year+'-'+month] = days;
					
					// Создаём каллендарь с новыми данными
					createCalendar();
					
					// Показываем каллендарь как только он создан
					showCalendar();

					if (is_first) {
						is_first = false;
						$('#site_cal').datepicker('setDate', new Date(curr_year, curr_month - 1, curr_day));
					}else{
						$('#site_cal').datepicker('setDate', new Date(year, month - 1, 1));
					};
				
				
			},
			'json'
    	);

	};
		
	// Удаление календаря
	function destroyCalendar()	{ 
		$('#site_cal').hide();
		$('#site_cal_loading').show();
		$('#site_cal').datepicker('destroy');
	};
	
	// Функция прячет аяксовую анимашку и показывает div с календарем
	function showCalendar(){ 
    	$('#site_cal_loading').hide();
    	$('#site_cal').show();
	};
	
	// функция, вызывающая конструктор datepicker из jQuery-UI для элемента #site_cal
	function createCalendar(){ 
    	$("#site_cal").datepicker({
			monthNames: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
			monthNamesShort: ['Янв', 'Фев', 'Мар', 'Апр', 'Май', 'Июн', 'Июл', 'Авг', 'Сен', 'Окт', 'Ноя', 'Дек'],
			dayNames: ['Воскресенье', 'Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота'],
			dayNamesMin: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
			nextText: 'Вперёд',
			prevText: 'Назад',
			firstDay: 1,
			gotoCurrent: false,
			shortYearCutoff: 20,
			dateFormat: 'yy-mm-dd',
			minDate: new Date(curr_year-1, curr_month-1, 1),
			maxDate: new Date(curr_year, 12, 31),

			// событие, срабатывающее при выборе пользователем другого месяца
        	onChangeMonthYear: function(year, month, inst) {
				curr_date = year+'-'+twoDigits(month);
            	
				if (is_first) {
            	    return false;
            	}

				if (typeof(cache_days[curr_date]) != 'object') {
					destroyCalendar();
               		loadMonthCalendar(year, twoDigits(month));		
				};
        	},
			
			// Событие, срабатывающее при клике на дату
        	onSelect: function(dateText, inst){
            	year = dateText.substr(0, 4);
            	month = dateText.substr(5, 2);
            	day = dateText.substr(8, 2);
				
            	if (typeof(cache_days[year+'-'+month]) == 'object') {
					if (in_array(parseInt(day,10), cache_days[year+'-'+month])) {
						// Если выбранная дата есть в кэше, то сделать на неё переход
						location.href = '/sor/?date='+year+'-'+month+'-'+day;
					}
				}
            	return false;
        	},

			// Событие вызывается перед отрисовкой каждого дня в месяце
        	beforeShowDay: function(date){	
            	var arr = new Array();  // Функция должна вернуть массив из трёх элементов
            	arr[0] = false; 		// Разрешать ли выбор даты
            	arr[1] = '';			// Какой css-класс назначить
            	arr[2] = '';			// Какую всплывающую подсказку показывать

				// Это ключевой момент. Если дата присутствует в кэше, то будет разрешён выбор этой даты
				if (in_array(date.getDate(), cache_days[date.getFullYear()+'-'+twoDigits(date.getMonth() + 1)])){
					
					var full_date = date.getFullYear() +'-'+ twoDigits(date.getMonth() + 1) +'-'+ twoDigits(date.getDate());
					arr[0] = true;
					for (var key in cache_prop[full_date]) {
						var num_li = parseInt(key) + 1;
						arr[2] = arr[2] + num_li + '. ' + cache_prop[full_date][key].sport + ' - ' + cache_prop[full_date][key].title + ' (' + cache_prop[full_date][key].city + ') \r\n';
					
					}
					
            	}
            	return arr; 
        	}
    	});
	};
	


	loadMonthCalendar(curr_year, curr_month);
	
	 });