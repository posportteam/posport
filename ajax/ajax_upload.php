<?
header('Content-Type: text/html; charset=utf-8');
require '../config.php';
require '../libs_php/db_connect.php';
require '../libs_php/user_connect.php';
require '../libs_php/classes.php';
require '../libs_php/functions.php';

switch ($_GET['action']){
	case "uploadAvatar":
	
		$allowedExt = array('jpg', 'jpeg', 'png', 'gif');
		$maxFileSize = 2 * 1024 * 1024; //2 MB
		$imgWidth = 500;
		$ext = end(explode('.', strtolower($_FILES['avatar_change_file']['name'])));
		
		if(!$_ENV['id_user']){
			$error = 1;
			$error_text = '{LT_ERROR_NO_AUTH}';
		}elseif(!isset($_FILES['avatar_change_file'])){
			$error = 2;
			$error_text = '{LT_ERROR_IMG_NO_FILE}';
		}elseif($maxFileSize <= $_FILES['avatar_change_file']['size']){
			$error = 3;
			$error_text = '{LT_ERROR_IMG_MORE_SIZE} '.$_FILES['avatar_change_file']['size'];
		}elseif(!is_uploaded_file($_FILES['avatar_change_file']['tmp_name'])){
			$error = 4;
			$error_text = '{LT_ERROR_IMG_NO_UPLOAD}';
		}elseif(!in_array($ext, $allowedExt)){
			$error = 5;
			$error_text = '{LT_ERROR_IMG_NO_EXT}';
		}else{

			//проверяем размер и тип файла
			
			$uploadDir = '../images/photo/tmp_photo/'; //папка для хранения врменных файлов
			$pieces = explode(".",$_FILES['avatar_change_file']["name"]);
			$newFileName = $_ENV['id_user'].'_'.(md5($pieces[0])).".".$pieces[1];
					
			$image = new SimpleImage();
			$image->load($_FILES['avatar_change_file']["tmp_name"]);
			$image->resizeToWidth($imgWidth);
			$image->save($uploadDir.$newFileName);
		};
	break;
	case "cropAvatar":
		if(!$_ENV['id_user']){
			$error = 1;
			$error_text = '{LT_ERROR_NO_AUTH}';
		}else{
			$uploadDir = '../images/photo/users/'.$_ENV['id_user'].'/'; //папка для хранения врменных файлов
			$newFileName = 'avatar.jpg';
			
			$image = new SimpleImage();
			$image->load($_POST['img_tmp']);
			$image->crop($_POST['img_x'],$_POST['img_y'],$_POST['img_w'],$_POST['img_h']);
			$image->resizeToWidth(170);
			$image->save($uploadDir.'avatar.jpg');
		};
	break;
};


// Формируем ответ
	$response = array(
			"img_tmp_dir"	=> $uploadDir,
			"img_tmp_name"	=> $newFileName,
			"error"			=> $error,
			"error_text"	=> $error_text,
	);
	print json_encode($response);
