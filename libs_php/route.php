<?
/*
	Класс-маршрутизатор для определения запрашиваемой страницы.
	цепляет классы контроллеров и моделей;
	создает экземпляры контролеров страниц и вызывает действия этих контроллеров.
*/

class Route{
	static function start(){
		// контроллер и действие по умолчанию
		$action_param = array();
		$action_name = 'index';

		// Разбиваем имя запроса в массив
		$routes = explode('/', $_SERVER['REQUEST_URI']);
		
		// получаем имя контроллера
		if (!empty($routes[1])){
		
			if(file_exists('./controllers/controller_'.strtolower($routes[1]).'.php')){ 		// Проверка контроллера
				$controller_name = $routes[1];
			}else{
				$pat_user = "/user\d{1,7}/i";
				$pat_id = "/id\d{1,7}/i";
				$pat_club = "/club\d{1,7}/i";
				$pat_org = "/org\d{1,7}/i";
				$pat_gal = "/gallery\d{1,10}/i";
				$pat_alb = "/album\d{1,10}/i";
				$pat_sor = "/sor\d{1,10}/i";
				$pat_art = "/article\d{1,7}/i";
				$pat_group = "/group\d{1,5}/i";
				
				if(preg_match($pat_id,$routes[1],$maches)){ 													//проверка по ID???? 
					$controller_name = 'users';
					$action_param = array('id_user'=>substr($routes[1],2));
				}elseif(preg_match($pat_user,$routes[1],$maches)){ 													//проверка по ID???? 
					$controller_name = 'users';
					$action_param = array('id_user'=>substr($routes[1],4));
				}elseif(preg_match($pat_group,$routes[1],$maches)){ 													//проверка по ID???? 
					$controller_name = 'groups';
					$action_param = array('id_group'=>substr($routes[1],5));
				}elseif(preg_match($pat_org,$routes[1],$maches)){
					$controller_name = 'org';
					$action_param = array('id_org'=>substr($routes[1],3));
				}elseif(preg_match($pat_sor,$routes[1],$maches)){
					$controller_name = 'sor';
					$action_name = 'view';
					$action_param = array('id_sor'=>substr($routes[1],3));
				}elseif(preg_match($pat_club,$routes[1],$maches)){
					$controller_name = 'club';
					$action_param = array('id_club'=>substr($routes[1],4));
				}elseif(preg_match($pat_art,$routes[1],$maches)){
					$controller_name = 'articles';
					$action_name = 'view';
					$action_param = array('id_art'=>substr($routes[1],7));
				}elseif(preg_match($pat_alb,$routes[1],$maches)){
					$controller_name = 'photo';
					$action_name = 'album';
					$action_param = array('id_photo'=>substr($routes[1],5));
				}elseif(preg_match($pat_gal,$routes[1],$maches)){
					$controller_name = 'photo';
					$action_param = array('id_photo'=>substr($routes[1],7));
				}else{
					$nick_q = ("SELECT id,controller FROM nicks WHERE nick='$routes[1]' LIMIT 1");
					$nick_r = mysql_query($nick_q);
					$nick = mysql_fetch_array($nick_r);
					if($nick['id'] && $nick['controller']){											// Проверка по нику
						$controller_name = $nick['controller'];
						$action_param = array('id'=>$nick['id']);
					};
				};
			};			
		}else{
			$controller_name = 'index';
		};

		
		// получаем имя экшена
		if (!empty($routes[2]) || !$action_name){
			
			$action = explode('?',$routes[2]);
			
			if (!empty($action[0])){
				$action_name = $action[0];
			};
			
			if (!empty($action[1])){
				parse_str($action[1],$action_param);
			};
		}

		// добавляем префиксы
		$model_name = 'model_'.$controller_name;
		$controller_name = 'controller_'.$controller_name;
		$action_name = 'action_'.$action_name;
		$controller_path = "./controllers/".strtolower($controller_name).'.php';
		
		//echo "<br><br><br>$controller_path";
		if(file_exists($controller_path)){
		
			// подцепляем файл с классом модели (файла модели может и не быть)
			$model_file = strtolower($model_name).'.php';
			$model_path = "./models/".$model_file;
			if(file_exists($model_path)){
				include "./models/".$model_file;
			}
		
			include $controller_path;
			
			// создаем контроллер
			$controller = new $controller_name;
		
			if(method_exists($controller, $action_name)){
				// вызываем действие контроллера с параметрами
				$controller->$action_name($action_param);
			}else{
				// здесь также разумнее было бы кинуть исключение
				Route::ErrorPage404();
			}
			
		}else{
			/*
			правильно было бы кинуть здесь исключение,
			но для упрощения сразу сделаем редирект на страницу 404
			*/
			Route::ErrorPage404();
		}
	}

	function ErrorPage404(){
        $host = 'http://'.$_SERVER['HTTP_HOST'].'/';
        header('HTTP/1.1 404 Not Found');
		header("Status: 404 Not Found");
		header('Location:'.$host.'404');
    }
};