	<!--- START TOOLBAR_START -->

	<div id='toolbar'>
		<div id='toolbar_panel'>
			<div id='sub_toolbar_panel'>
				<div class='toolbar-section sor'>
					{LT_NO_SOR_NOW}
				</div>
				<div class='toolbar-section sports'>
					{SPORT}<br>
					{LT_REGION}: {REGION}
				</div>
				<div class='toolbar-section auth' >
					<div class='buttonset' style='margin: 0 auto;'>
					<? if($_ENV[id_user]){?>
					<!-- IF USER_ACTIVE -->
					
						<button id='loginout'>{LT_BUT_EXIT}</button>
						
					<? }else{ ?>
					<!-- ELSE -->
					
						
							<button id='but_auth_users'>{LT_BUT_ENTER}</button> 
							<button id='but_reg_users' href="/reg">{LT_BUT_REG}</button>
						
						
					<?};?>
					<!-- ENDIF -->
					</div>
				</div>
				<div class='toolbar-section cs'>
					<button class='but_cart' href="http://shop.posport.ru">{LT_BUT_LINK_SHOP}</button>
					<button class='but_home' href="http://posport.ru">{LT_BUT_LINK_PORTAL}</button>
					<button class='but_my_small'  href="">{LT_BUT_LINK_MYSETTINGS}</button>
				</div>
				<div class='toolbar-section toolbar_set but_set'>
				</div>
				<div class='toolbar-section search'>
					<form method='get' id='searchform' action='?page=search'>
						<div>
							<input type='search' name='search' id='search' />
							<button type='submit' id='searchsubmit' class='but_search'/>{LT_LABLE_SEARCH}</button>
						</div>
					</form>
				</div>
				
			</div>
		</div>
		<div id='toolbar_settings'>
			<div id='sub_toolbar_settings'>
				<div class='toolbar-section sor'>
					{LT_SOR_SOON}
				</div>
				<div class='toolbar-section sports'>
					<form action='{CUR_URL}' method='POST'><input type='hidden' name='set_geo' value='1'>
						
						<div id='div_set_site_sport'>
							{SELECT_SPORTS}
						</div>
					
						<div id='div_set_site_region' {DIV_REGION_HID}>
							{SELECT_REGION}
						</div>
						<div class='buttonset'>
							<button type='submit' class='but_save_small' id='save_toolbar_sports'>{LT_BUT_SAVE}</button>
							<input type='checkbox' id='set_site_myregion' name='site_myregion' value='1' {MY_REGION_CKECKED}>
							<label for='set_site_myregion'>{LT_REGION_HOME}</label>
						</div>
					</form>
				</div>
				<div class='toolbar-section auth' >
					
					<? if($_ENV[id_user]){?>
					<!-- IF USER_ACTIVE -->
					
						<div class='stat_count'>{LT_NUM_SPORTSMENS}: {NUM_SPORTSMENS}</div>
						<div class='stat_count'>{LT_NUM_USERS}: {NUM_USERS}</div>			
						
					<? }else{ ?>
					<!-- ELSE -->	
					<form action='ajax_auth.php?action=auth' method='POST' id='auth' name='auth'>
						<label for='auth_telephone'>{LT_LABLE_PHONE}</label>
							<input type='text' value='' id='auth_phone' name='auth_phone' class='phone' pattern='[0-9]{10}' required>
						<label for='auth_pass'>{LT_LABLE_PASS}</label>
							<input type='password' value='' id='auth_pass' name='auth_pass' required>
						<div id='auth_loading'>{LT_AUTH_WAIT}</div>
						<div class='buttonset' style='margin-top: 7px;'>
							<input type='submit' value='{LT_BUT_ENTER_ON_SAIT}'>
						</div>
					</form>
					<?};?>
					<!-- ENDIF -->
					
				</div>
			</div>
		</div>
	</div>

<!--- TOOLBAR_FINISH -->