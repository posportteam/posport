<div class='sor_event {SOR_CLASS_STATUS}' data-id-sor='{SOR_ID}'>
	<div class='sor'>
		<div class='sor_logo_sport'>
			<div class='logo_sport avatar_40 logo_sport_{SOR_SPORT_ID} sport_type_{SOR_SPORT_TYPE_ID}'>
			&nbsp;
			</div>
		</div>
		<div class='sor_info'>
			<b>{SOR_DATE} - {SOR_TITLE}</b><br>
			{LT_SPORT}: {SOR_SPORT} {SOR_DISTS}<br>
			{LT_ADRESS}: {SOR_CITY}{SOR_ZAL}
		</div>
		<div class='sor_status'>
			{SOR_STATUS}
		</div>
	</div>
	<div class='buttonset_sor'>
		{SOR_BUT_VIEW}
		{SOR_BUT_EDIT}
		{SOR_BUT_POLOJ}
		{SOR_BUT_REG}
		{SOR_BUT_REQ}
		{SOR_BUT_USERS}
		{SOR_BUT_ONLINE}
		{SOR_BUT_RESULT}
		{SOR_BUT_PHOTO}
		{SOR_BUT_VIDEO}
	</div>
</div>