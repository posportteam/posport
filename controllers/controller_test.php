<?php
class Controller_test extends Controller{
	function __construct(){
		$this->model = new Model_test();
		$this->view = new View_page();
	}
	
	
	function action_index(){
		$data = $this->model->get_data();
		$this->view->generate_page('test', $data);
	}

}
?>