<div class='article_preview {CLASS_ART_VISIBLE}'  data-id-art='{ART_ID}'>
	<div class='art_author'>
		<a href='/user{AUTHOR}'>
		<div class='avatar_50' style='background: url("{AVATAR}") no-repeat center; background-size: cover;	-webkit-background-size: cover;	-0-background-size: cover;	-moz-background-size: cover;'>
			&nbsp;
		</div>
		</a>
	</div>
	<div class='art_info' style='display:{DISPLAY_INFO};'>
		<div class='art_author_name'><a href='/user{AUTHOR}'>{AUTHOR_NAME}</a></div>
		<div class='art_date'>{LT_DATE_RELEASE}: {DATE}</div>
		<? if($_ENV['id_user']){ ?>
		<div>
			<a href='#' class='{ART_FAV_CLASS} art_icon' data-id-art='{ART_ID}' data-event='{ART_FAV_EVENT}'>{ART_FAV_TITLE}</a>
			{BUTTONSET_EDIT_ART}
		</div>
		<?};?>
	</div>
	<div class='art_rec' style='display:{DISPLAY_REC};'>{LT_ART_DELETING}. <a class='artRec' data-event='artRec'>{LT_RECOVERY_GO}!</a></div>
	<div class='loading'></div>
</div>

<h4>{ART_TITLE}</h4>

<div class='art_text' id='art_text' style='display:{DISPLAY_CONTENT};'>
	{ART_TEXT}
</div>

<div id='comments_in_art' name='art_comments' data-id-art='{ART_ID}' style='display:{DISPLAY_CONTENT};'>
	{COMMENTS}
</div>