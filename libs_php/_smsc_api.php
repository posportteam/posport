<?php
// SMSC.RU API (www.smsc.ru)

define("SMSC_LOGIN", "posport.ru");		// ����� �������
define("SMSC_PASSWORD", "bobulu18");	// ������ ��� MD5-��� ������ � ������ ��������
define("SMSC_POST", 0);					// ������������ ����� POST (������ ����� curl)
define("SMSC_HTTPS", 0);				// ������������ HTTPS ��������
define("SMSC_DEBUG", 0);				// ���� �������

// ������� �������� SMS
//
// ������������ ���������:
//
// $phones - ������ ��������� ����� ������� ��� ����� � �������
// $mes - ������������ ��������� 
//
// �������������� ���������:
//
// $translit - ���������� ��� ��� � �������� (1 ��� 0)
// $time - ����������� ����� �������� (DDMMYYhhmm, h1-h2, 0ts, +m)
// $timezone - ������� ���� ������������ ������
// $id - ������������� ���������
// $flash - flash ��������� (1 ��� 0)
// $sender - ��� ����������� (Sender ID)
// $charset - ��������� ��������� (utf-8 ��� koi8-r), �� ��������� ������������ windows-1251
//
// ���������� ID ��������� � ������ �������� �������� ���� -<��� ������> ��� false � ������ ������

function send_sms($phones, $mes, $translit = 0, $time = 0, $id = 0, $flash = 0, $sender = false, $charset = "utf-8", $timezone = "5")
{
	$result = _smsc_send_cmd("send", "phones=".urlencode($phones)."&mes=".urlencode($mes).
					"&translit=$translit&id=$id&flash=$flash".($sender === false ? "" : "&sender=".urlencode($sender))."&charset=$charset".($time ? "&time=".urlencode($time)."&tz=$timezone" : ""));

	if (preg_match("/^OK - (\d+) SMS, ID - (\d+)/", $result, $m))
	{
		if (SMSC_DEBUG)
			echo "��������� ���������� �������. ����� ���������� SMS: $m[1]. ID ��������� - $m[2]\n";

		return $m[2];
	}
	elseif (preg_match("/^ERROR = (\d)(.*?)(?:, ID - (\d+))?$/", $result, $m))
	{
		if (SMSC_DEBUG)
			echo "������ � $m[1]$m[2]", isset($m[3]) ? ", ID: ".$m[3] : "", "\n";

		return -$m[1];
	}

	if (SMSC_DEBUG)
		echo "������ ������ �������������� ����� - ", $result, "\n";

	return false;
}

// SMTP ������ ������� �������� SMS

function send_sms_mail($phones, $mes, $translit = 0, $time = 0, $id = 0, $flash = 0, $sender = "", $charset = "windows-1251", $timezone = "")
{
	return mail("send@send.smsc.ru", "", SMSC_LOGIN.":".SMSC_PASSWORD.":$id:$time".($timezone !== "" ? ",".$timezone : "").":$translit,$flash,$sender:$phones:$mes", "Content-Type: text/plain; charset=$charset\n");
}

// ������� ��������� ��������� SMS
//
// ������������ ���������:
//
// $phones - ������ ��������� ����� ������� ��� ����� � �������
// $mes - ������������ ��������� 
//
// �������������� ���������:
//
// $translit - ���������� ��� ��� � �������� (1 ��� 0)
// $sender - ��� ����������� (Sender ID)
//
// ���������� ��������� ��������� ���� -<��� ������> ��� false � ������ ������

function get_sms_cost($phones, $mes, $translit = 0, $sender = false, $charset = "")
{
	$result = _smsc_send_cmd("send", "phones=".urlencode($phones)."&mes=".urlencode($mes).($sender === false ? "" : "&sender=".urlencode($sender))."&charset=$charset&translit=$translit&cost=1");

	if (preg_match("/^([\d\.]+) \((\d+) SMS\)/", $result, $m))
	{
		if (SMSC_DEBUG)
			echo "��������� ��������: $m[1] ���. ����� SMS: $m[2]\n";

		return $m[1];
	}
	elseif (preg_match("/^ERROR = (\d)(.*?)$/", $result, $m))
	{
		if (SMSC_DEBUG)
			echo "������ � $m[1]$m[2]\n";

		return -$m[1];
	}

	if (SMSC_DEBUG)
		echo "������ ������ �������������� ����� - ", $result, "\n";

	return false;
}

// ������� �������� ������� ������������� SMS
//
// $id - ID c��������
// $phone - ����� ��������
//
// ���������� �������� ��� ������� ��� false � ������ ������

function get_status($id, $phone) {
	$result = _smsc_send_cmd("status", "phone=".urlencode($phone)."&id=".$id);

	if (preg_match("/^Status = (-?\d+)(?:, check_time = (.+))?$/", $result, $m))
	{
		if (SMSC_DEBUG)
			echo "������ SMS = $m[1]", isset($m[2]) ? ", ����� ��������� ������� - $m[2]" : "", "\n";

		return $m[1];
	}
	elseif (preg_match("/^ERROR = (\d)(.*)$/", $result, $m))
	{
		if (SMSC_DEBUG)
			echo "������ � $m[1]$m[2]\n";

		return false;
	}

	if (SMSC_DEBUG)
		echo "������ ������ �������������� ����� - ", $result, "\n";

	return false;
}

// ������� ��������� �������
//
// ��� ����������
//
// ���������� ����� ������� ��� false � ������ ������

function get_balance() {
	$result = _smsc_send_cmd("balance");

	if (is_numeric($result))
	{
		if (SMSC_DEBUG)
			echo "����� �� �����: ", $result, "\n";

		return $result;
	}
	elseif (preg_match("/^ERROR = (\d)(.*)$/", $result, $m))
	{
		if (SMSC_DEBUG)
			echo "������ � $m[1]$m[2]\n";

		return false;
	}

	if (SMSC_DEBUG)
		echo "������ ������ �������������� ����� - ", $result, "\n";

	return false;
}


// ���������� �������

// ������� ������ �������. ��������� URL � ������ 3 ������� ������.

function _smsc_send_cmd($cmd, $arg = "") {
	$url = (SMSC_HTTPS ? "https" : "http")."://smsc.ru/sys/$cmd.php?login=".urlencode(SMSC_LOGIN)."&psw=".urlencode(SMSC_PASSWORD)."&".$arg;

	$i = 0;
	do {
		if ($i)
			sleep(2);

		$ret = _smsc_read_url($url);
	}
	while ($ret == "" && ++$i < 3);

	return $ret;
}

// ������� ������ URL. ��� ������ ������ ���� ��������:
// curl ��� fsockopen (������ http) ��� �������� ����� allow_url_fopen ��� file_get_contents

function _smsc_read_url($url) {
	$ret = "";
	$post = SMSC_POST || strlen($url) > 2000;

	if (function_exists("curl_init"))
	{
		$c = curl_init();

		if ($post) {
			list($url, $post) = explode('?', $url, 2);
			curl_setopt($c, CURLOPT_POST, true);
			curl_setopt($c, CURLOPT_POSTFIELDS, $post);
		}

		curl_setopt($c, CURLOPT_URL, $url);
		curl_setopt($c, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($c, CURLOPT_CONNECTTIMEOUT, 10);
		curl_setopt($c, CURLOPT_TIMEOUT, 10);
		curl_setopt($c, CURLOPT_SSL_VERIFYPEER, 0);

		$ret = curl_exec($c);
		curl_close($c);
	}
	elseif (!SMSC_HTTPS && function_exists("fsockopen"))
	{
		$m = parse_url($url);

		$fp = fsockopen($m["host"], 80, $errno, $errstr, 10);

		if ($fp) {
			fwrite($fp, ($post ? "POST $m[path]" : "GET $m[path]?$m[query]")." HTTP/1.1\r\nHost: smsc.ru\r\nUser-Agent: PHP".($post ? "\r\nContent-Type: application/x-www-form-urlencoded\r\nContent-Length: ".strlen($m['query']) : "")."\r\nConnection: Close\r\n\r\n".($post ? $m['query'] : ""));

			while (!feof($fp))
				$ret = fgets($fp, 100);

			fclose($fp);
		}
	}
	else
		$ret = file_get_contents($url);

	return $ret;
}

?>
