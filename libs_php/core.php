<?
class Model{
	function __construct(){
		$this->tpl = new Tpl();
		$this->sql_query = new sql_query();
		$this->select_linklist = new select_linklist();	
		$this->selecter = new selecter();
	}
	/*
		Модель обычно включает методы выборки данных, это могут быть:
			> методы нативных библиотек pgsql или mysql;
			> методы библиотек, реализующих абстракицю данных. Например, методы библиотеки PEAR MDB2;
			> методы ORM;
			> методы для работы с NoSQL;
			> и др.
	*/

	// метод выборки данных
	public function get_data(){
		// todo
	}
};

class Controller{
	public $model;
	public $view_page;
	
	function __construct(){
		$this->view = new View_page();
		$this->tpl = new Tpl();
		$this->sql_query = new sql_query();
	}
	
	// действие (action), вызываемое по умолчанию
	function action_index(){
		// todo	
	}
};


// Клас для подключения шаблона с заменой переменных
class Tpl{
	function generate($tpl,$data=null,$ajax=null){
		if($ajax){
			$prefix = '..';
		}else{
			$prefix = '.';
		};
		ob_start();
			include $prefix.'/skins/'.SKIN.'/tpl/'.$tpl.'.tpl';
			$page = ob_get_contents();
			if($data){
				$page = strtr($page,$data);
			};
		ob_end_clean();
		return $page;
	}
}


class View_page{
	
	//public $template_view; // здесь можно указать общий вид по умолчанию.
	
	/*
	$content_file - виды отображающие контент страниц;
	$template_file - общий для всех страниц шаблон;
	$data - массив, содержащий элементы контента страницы. Обычно заполняется в модели.
	динамически подключаем общий шаблон (вид),	внутри которого будет встраиваться вид	для отображения контента конкретной страницы.
	*/
	
	function generate_page($content_view, $data=null, $title){
		$this->modules = new Modules();
		$this->tpl = new Tpl();	
		
		if($data['data-type-search']){
			$FILTER_TITLE = $this->tpl->generate('filter_title');
		};
		
		if(!$title){$title = '{LT_TITLE}';};
		
		$data = array(
			'{TITLE}'	=> $title,
			'{TOOLBAR}' => $this->modules->generate('toolbar'),
			'{HEADER}' => $this->modules->generate('header'),
			'{CONTENT_PAGE}' => $this->tpl->generate($content_view, $data),
			'{FILTER}' => $data['{FILTER}'],
			'{FILTER_TITLE}' => $FILTER_TITLE,
			'{SIDEBAR}' => $this->modules->generate('sidebar'),
			'{FOOTER}' => $this->modules->generate('footer')
		);
			
		$page = $this->tpl->generate('index', $data);
		
		// Подключаем языковой файл, замена переменных языка с литером LT_
		require_once './language/'.LANGUAGE.'.php'; 
		// Выводим страницу
		echo strtr($page,$lang);

	}
};

// Класс для поключения модулей
class Modules{
	function generate($module){
		$model_file = 'model_'.strtolower($module).'.php';
		$model_path = "./models/".$model_file;
		if(file_exists($model_path)){
			include $model_path;
		}
		
		$controller_name = 'controller_'.strtolower($module);
		$controller_path = './controllers/'.$controller_name.'.php';
		if(file_exists($controller_path)){
			include $controller_path;
			$controller = new $controller_name;
			return $controller->action();
		}
	}
};

?>