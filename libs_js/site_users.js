$(document).ready(function(){

	$( "#user_edit" ).tabs();

	setnull_for_obj($('div.careers_org_edit.new'));
	setnull_for_obj($('div.careers_sport_edit.new'));
	setnull_for_obj($('div.rang_edit.new'));
	init_autocomplete('city');
	init_autocomplete('sport');
	init_autocomplete('org');
	
	// pre-submit callback 
	function showRequest(formData, jqForm, options) { 
		// formData is an array; here we use $.param to convert it to a string to display it 
		// but the form plugin does this for you automatically when it submits the data 
		//var queryString = $.param(formData); 
		// jqForm is a jQuery object encapsulating the form element.  To access the 
		// DOM element for the form do this: 
		// var formElement = jqForm[0]; 
		//alert('About to submit: \n\n' + queryString); 
		return true; 
	} 
	 
	// post-submit callback 
	function showResponse(responseText, statusText, xhr, $form)  { 
		$("#page_response").show().text(responseText).fadeOut(4000);
		location.href='#';
	} 

	// Отправка заполненной формы, полное сохранение изменений
	$('#form_user_edit').submit(function() { 
        // inside event callbacks 'this' is the DOM element so we first 
        // wrap it in a jQuery object and then invoke ajaxSubmit 
        $(this).ajaxSubmit({ 
			beforeSubmit:  showRequest,  // pre-submit callback 
			success:       showResponse  // post-submit callback 
	 
			// other available options: 
			//url:       url         // override for form's 'action' attribute 
			//type:      type        // 'get' or 'post', override for form's 'method' attribute 
			//dataType:  null        // 'xml', 'script', or 'json' (expected server response type) 
			//clearForm: true        // clear all form fields after successful submit 
			//resetForm: true        // reset the form after successful submit 
			//target:        '#output2',   // target element(s) to be updated with server response 
			// $.ajax options can be used here too, for example: 
			//timeout:   3000 
		}); 
 
        return false; 
    }); 

	$('select.user_group').each(function(){
		var id_group = $(this).attr('data-id-group');
		$(this).val(id_group);
	});
	
	$('div.users_preview input, div.users_preview select').change(function(){
		var div_preview = $(this).parents('div.users_preview');
		div_preview.addClass('changed');
	});
	
	
	/* $('button.save_user').click(function(){
		var div_preview = $(this).parents('div.users_preview');
		
		var data = {
			id_user		:	$(this).attr('data-id-user'),
			name		:	div_preview.find('input.name').val(),
			second_name	:	div_preview.find('input.second_name').val(),
			phone		:	div_preview.find('input.phone').val(),
			id_group	:	div_preview.find('select.user_group').val()
		};
		
		$.ajax({  
			type: "POST",  
			url: "/ajax/ajax_users.php?action=userSave",  
			data: data,
			beforeSend: function(){
				div_preview.find('div.loading').show();
			},
			success: function(response){
				div_preview.find('div.loading').hide();
				div_preview.removeClass('changed');
			}
		});
		
	}); */
	
	$('#user_edit').on('change','input.careers_city, input.careers_sport',function(){
		select_city_sport($(this));
	});
	
	$('div.careers_sport_edit select.club').each(function(){
		select_city_sport($(this));
	});
	
	
	// Сохранялки для карьер и званий
	$('#user_edit').on('click', 'button.save_career_sport', function(){
		var obj_career_sport = $(this).parent('div');
		
		var for_city = obj_career_sport.parent('div').find('input.careers_city').attr('for');
		var city = $('#'+for_city).val();
		
		var for_sport = obj_career_sport.parent('div').find('input.careers_sport').attr('for');
		var sport = $('#'+for_sport).val();
		
		var club = obj_career_sport.find('select.club').val();
		var trener = obj_career_sport.find('select.trener').val();
		var date_in = obj_career_sport.find('input.datepicker_from').val();
		var date_out = obj_career_sport.find('input.datepicker_to').val();

		if(!city || !sport || club==0 || trener==0 || !date_in){
			alert('Uncorrect data !(');
		}else{
			
			if(obj_career_sport.hasClass('new')){
				// Убиваем обработчики иначе будут ошибки с обращениями к оригиналу от копии
				$('input.city, input.sport').autocomplete("destroy");
				$('input.datepicker_from, input.datepicker_to').datepicker("destroy").attr('id','');
				
				// Копируем, вставляем в начало списка после заголовка, удаляем класс new
				var new_obj = obj_career_sport.clone(true).insertAfter("#h3_career_sport").removeClass('new');
				
				// Присваеваем новые ID и FOR
				var rand_careers_sport = randomNumber(1000,9999);
				// Для спорта
				new_obj.find('input.sport').attr('for','user_careers_sport_sport_'+rand_careers_sport);
				$('#user_careers_sport_sport_0').attr('id','user_careers_sport_sport_'+rand_careers_sport);
				// Для города
				new_obj.find('input.city').attr('for','user_careers_sport_city_'+rand_careers_sport);
				$('#user_careers_sport_city_0').attr('id','user_careers_sport_city_'+rand_careers_sport);
				
				// Устанавливаем у нового объекта по умолчанияю списки
				var new_obj_select_club = new_obj.find('select.club');
				var new_obj_select_trener = new_obj.find('select.trener');
				
				new_obj_select_club.attr('data-defval',club);
				new_obj_select_trener.attr('data-defval',trener);
				select_city_sport(new_obj_select_club);
				
				// Зануляем щаблон
				setnull_for_obj($('div.careers_sport_edit.new'));

				// Инициализируем занового Атозаполнение для города и спорта, которые мы ранее убили, а так же инициализируем календари
				init_autocomplete('city');
				init_autocomplete('sport');
				init_datepicker();
				

			};
		};
		
		return false;
	});
	
	$('#user_edit').on('click', 'button.save_rang', function(){
	 	var obj_rang = $(this).parent('div');
		
		var for_sport = obj_rang.parent('div').find('input.sport').attr('for');
		var sport = $('#'+for_sport).val();
		
		var date = obj_rang.find('input.datepicker').val();
		var id_rang = obj_rang.find('select.select_rangs').val();

		if(!sport || id_rang==0 || !date){
			alert('Uncorrect data !(');
		}else{
			if(obj_rang.hasClass('new')){
				$('input.sport').autocomplete("destroy");
				$('input.datepicker').datepicker("destroy").attr('id','');
				
				var new_obj = obj_rang.clone(true).insertAfter("#h3_rangs").removeClass('new').find('select.select_rangs').val(id_rang);
				
				// Присваеваем новые ID и FOR
				var rand_careers_sport = randomNumber(1000,9999);
				// Для спорта
				new_obj.find('input.sport').attr('for','user_careers_rangs_sport_'+rand_careers_sport);
				$('#user_careers_rangs_sport_0').attr('id','user_careers_rangs_sport_'+rand_careers_sport);
				
				new_obj.find('select.select_rangs').val(id_rang);
				setnull_for_obj($('div.rang_edit.new'));

				init_autocomplete('sport');
				init_datepicker();
				
			};
		};

		return false;		
	});
	
	$('#user_edit').on('click', 'button.save_career_org', function(){
		var obj_career_org = $(this).parent('div');
		
		var club = obj_career_org.find('input.id_club').val();
		var org = obj_career_org.find('input.id_org').val();
		
		var org_post = obj_career_org.find('input.careers_post').val();
		var access_type = obj_career_org.find('select.select_accessType').val();
		var date_in = obj_career_org.find('input.datepicker_from').val();
		var date_out = obj_career_org.find('input.datepicker_to').val();
	
		if((!org && !club) || !org_post|| !date_in || access_type==0){
			alert('Uncorrect data !(');
		}else{
			if(obj_career_org.hasClass('new')){
				$('input.city, input.org').autocomplete("destroy");
				$('input.datepicker_from, input.datepicker_to').datepicker("destroy").attr('id','');
				
				var new_obj = obj_career_org.clone(true).insertAfter("#h3_career_org").removeClass('new').find('select.select_accessType').val(access_type);
				// Присваеваем новые ID и FOR
				var rand_careers_org = randomNumber(1000,9999);
				// Для города
				new_obj.find('input.city').attr('for','user_careers_org_city_'+rand_careers_org);
				$('#user_careers_org_city_0').attr('id','user_careers_org_city_'+rand_careers_org);

				
				setnull_for_obj($('div.careers_org_edit.new'));

				new_obj.find('select.select_accessType').val(access_type);
				
				init_autocomplete('city');
				init_autocomplete('org');
				init_datepicker();
				
			};
		}; 
		
		return false;	
	});

	
	function select_city_sport(obj){	
		var for_city = obj.parent('div').find('input.careers_city').attr('for');
		var city = $('#'+for_city).val();
		
		var for_sport = obj.parent('div').find('input.careers_sport').attr('for');
		var sport = $('#'+for_sport).val();
		
		var obj_club = obj.parent('div').find('select.club');
		
		var data = {
			city		:	city,
			sport		:	sport
		};
		
		linklist(obj_club,1,1, data);
		
	};
	
	
	function init_autocomplete(type,city){
		$('input.'+type).autocomplete({
			source: "/ajax/ajax_autocomplete.php?action=show_"+type+"&type=token&id_city="+city,
			delay: 300,
			minLength: 0,
			search: function( event, ui ) {
				var id_hidden_obj = $(this).attr('for');
				$('#'+id_hidden_obj).val('');
				
				if(type=='org'){
					$(this).parent('div').find('input.id_org').val('');
					$(this).parent('div').find('input.id_club').val('');
				};

			},
			select: function(event,ui) {
				var id_hidden_obj = $(this).attr('for');
				$('#'+id_hidden_obj).val(ui.item.id);

				if(type=='org'){
					$(this).parent('div').find('input.id_org').val(ui.item.id_org);
					$(this).parent('div').find('input.id_club').val(ui.item.id_club);
				}else if(type=='city' || type=='sport' ){
					select_city_sport($(this));
				};
				
			}
		});
	};


	
	
	$('#avatar_change_but').click(function(){
		$('#avatar_change').dialog({
			width: 535,
            modal: true,
			position: 'center',
			buttons: {
                "Сохранить": function() {
					var img_x = $('#avatar_tmp_preview_img_x').val();
					var img_y = $('#avatar_tmp_preview_img_y').val();
					var img_w = $('#avatar_tmp_preview_img_w').val();
					var img_h = $('#avatar_tmp_preview_img_h').val();
					var img_tmp = $('#avatar_tmp_preview_img').attr('src');
					
					
					$.ajax({
						type: "POST",  
						url: "/ajax/ajax_upload.php?action=cropAvatar",  
						data: {img_tmp:img_tmp, img_x:img_x, img_y:img_y, img_w:img_w, img_h:img_h},  
						beforeSend: function(){
							$('#avatar_tmp_preview').find('div.loading').show();
						},
						success: function(response){ 
							$('#avatar_tmp_preview').find('div.loading').hide();
							var obj = JSON.parse(response);
							var rend = Math.floor((Math.random()*1000)+1);
							
							$('#user_avatar').attr('src', '/images/loading.gif').attr('src', obj.img_tmp_dir + obj.img_tmp_name + '?new=' + rend);
						}
					});
					$( this ).dialog( "close" );
                }
            }
		});
	});

	$('#avatar_change_form').ajaxForm({
		baforeSend: function(){
			$('#avatar_tmp_preview').find('div.loading').show();
			$('#avatar_tmp_preview_div').hide().empty();
		},
		complete: function(response){
			$('#avatar_tmp_preview').find('div.loading').hide();
			var obj = JSON.parse(response.responseText);
			var img_tmp_teg = '<img id="avatar_tmp_preview_img" class="photo" src="' + obj.img_tmp_dir + obj.img_tmp_name + '">';
			$('#avatar_tmp_preview_div').html(img_tmp_teg).show();
			$('#avatar_tmp_preview_img').Jcrop({ // Привязываем плагин JСrop к изображению
				aspectRatio: 0,
				onChange: updateCoords,
				onSelect: updateCoords
			});
		}
	});

	function updateCoords(c){
		$('#avatar_tmp_preview_img_x').val(c.x);
		$('#avatar_tmp_preview_img_y').val(c.y);
		$('#avatar_tmp_preview_img_w').val(c.w);
		$('#avatar_tmp_preview_img_h').val(c.h);
	};
	
});