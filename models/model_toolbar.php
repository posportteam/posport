<?php
class Model_toolbar extends Model{
	public function get_data(){	
		
		$city_checked = '';
		$region_hid = '';
		
		// Проверка города
		if($_ENV['site_city']){
			$city_q = "SELECT title FROM geo_city WHERE id_city='$_ENV[site_city]'";
			$city_r = mysql_query($city_q);
			$city = mysql_fetch_array($city_r);
			$region = $city['title'];
			
			// Если используется домашний регион
			if($_ENV['site_city']==$ip_id_city['id_city']){
				$city_checked = 'checked';
				$region_hid = 'hidden';
			};
		}elseif($_ENV['site_obl']){
			$obl_q = "SELECT title FROM geo_obl WHERE id_obl='$_ENV[site_obl]'";
			$obl_r = mysql_query($obl_q);
			$obl = mysql_fetch_array($obl_r);
			$region = $obl['title'];
		}elseif($_ENV['site_fedok']){
			$fedok_q = "SELECT title FROM geo_fedok WHERE id_fedok='$_ENV[site_fedok]'";
			$fedok_r = mysql_query($fedok_q);
			$fedok = mysql_fetch_array($fedok_r);
			$region = $fedok['title']." {LT_FEDOKR_L}";
		}else{
			$region = '{LT_COUNTRY_NOW}';
		};

			
		// Количество спортсменов
		$count_q = mysql_query("SELECT count(*) FROM users");
		$count = mysql_fetch_array($count_q);
		$num_sportsmens = $count[0];
		
		// Количество пользователей
		$count_q = mysql_query("SELECT count(*) FROM users WHERE users.pass!=''");
		$count = mysql_fetch_array($count_q);
		$num_users = $count[0];
		
		// Проверка спорта
		if($_ENV['site_sport']){
			$sport_r = mysql_query("SELECT title FROM sports WHERE id_sport='$_ENV[site_sport]' LIMIT 1");
			$sport = mysql_fetch_array($sport_r);
			$sport=$sport['title'];
			
		}elseif($_ENV['site_sport_type']){
			$sport_type_r = mysql_query("SELECT title FROM sport_type WHERE id_sport_type='$_ENV[site_sport_type]' LIMIT 1");
			$sport_type = mysql_fetch_array($sport_type_r);
			$sport=$sport_type['title'];
		}else{
			$sport='{LT_SPORTS_ALL}';
		};
		

		
		$data = array(
				'{SPORT}' 				=> $sport,
				'{NUM_SPORTSMENS}' 		=> $num_sportsmens,
				'{NUM_USERS}' 			=> $num_users,
				'{REGION}' 				=> $region,
				'{MY_REGION_CKECKED}' 	=> $city_checked,
				'{DIV_REGION_HID}' 		=> $region_hid,
				'{SELECT_SPORTS}' 		=> $this->select_linklist->generate_sport(array('sport_type' => $_ENV[site_sport_type], 'sport' => $_ENV[site_sport])),				
				'{SELECT_REGION}' 		=> $this->select_linklist->generate_region(array('fedok' => $_ENV[site_fedok], 'obl' => $_ENV[site_obl], 'city' => $_ENV[site_city])),
				'{CUR_URL}' 			=> $_ENV['cur_url']
			);
			
		return $data;
	}
}
?>
