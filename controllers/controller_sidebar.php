<?php
class Controller_sidebar{
	function __construct(){
		$this->model = new Model_sidebar();	
		$this->tpl = new Tpl();	
	}

	function action(){
		$data = $this->model->get_data();
		$page = $this->tpl->generate('sidebar', $data);
		return $page;
	}
}

