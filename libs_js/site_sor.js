$(document).ready(function(){

	$('#but_user_add').click(function(){
		if($('#user_add input, #user_add select.team, #user_add select.rang').val()){
			$('#form_user_add').ajaxSubmit({ 
				beforeSubmit:  function(formData, jqForm, options){
					//$('#sor_teams').hide();
					//$('#loading_sor_add').show();
				},
				success:       function(responseText, statusText, xhr, $form){
					alert(responseText);
				}
			});
		}else{
			alert('Данные заполнены не верно!');
		};
		return false;
	});

	$('#user_add input.name').autocomplete({
		source: function(request, response) {
			var id_sport = $('#user_add input.id_sport').val();
			$.ajax({
                type: "POST",
                url: "/ajax/ajax_autocomplete.php?action=show_user",
                data: {
					id_sport	: id_sport,
					term		: request.term
					},
                dataType: "json",
                success: function (data) {
                    response(data);
                },
                error: function (result) {
                    alert("Seems like error occured. Please reload page and try again.");
                }
            });
		},
		delay: 300,
		minLength: 1,
		search: function(event,ui) {
			
			$('#user_add input, #user_add select').removeAttr('disabled');
			$('#user_add input.id_user, #user_add input.second_name, #user_add input.born').val('');
			$('#user_add select.rang, #user_add select.team').val(0);
		},
		select: function(event,ui) {
			$('#user_add input.id_user').val(ui.item.id).attr('disabled','true');
			$('#user_add input.second_name').val(ui.item.second_name).attr('disabled','true');
			$('#user_add select.sex').val(ui.item.sex).attr('disabled','true');
							
			if(ui.item.rang){$('#user_add select.rang').val(ui.item.rang).attr('disabled','true');};
			if(ui.item.born!='00.00.0000'){$('#user_add input.born').val(ui.item.born).attr('disabled','true');};
			
			$('#user_add input.weight').focus();
			
		}
	});

	
	$('#user_add').on('change','.name', function(e){
		if($(this).val()){
			$('#user_add input.id_user').attr('disabled','disabled');
		}else{
			$('#user_add input.id_user').removeAttr('disabled');
		};
	});
	
	
	$('#user_add').on('keyup','.id_user', function(e){
		if(!(48<=e.which && e.which<=57) && e.which!=13){
			setnull_for_obj($('#user_add'));
		};
	});
	

	
	$('#user_add').on('change','.id_user', function(e){
		var val = parseInt($(this).val());
		var id_sport = $('#user_add input.id_sport').val();
		if(val){
			var data = {
				id			: val,
				id_sport	: id_sport
			};
			$.ajax({
				type: "POST",
				url: "/ajax/ajax_autocomplete.php?action=show_user",
				data: data,
				beforeSend: function(){
							
				},
				success: function(response){
					var obj = JSON.parse(response);
					if(obj.length){
						$(this).val(obj[0].id);
						$('#user_add input.name').val(obj[0].name).attr('disabled','true');
						$('#user_add input.second_name').val(obj[0].second_name).attr('disabled','true');
						$('#user_add select.sex').val(obj[0].sex).attr('disabled','true');
								
						if(obj[0].rang){$('#user_add select.rang').val(obj[0].rang).attr('disabled','true');};
						if(obj[0].born!='00.00.0000'){$('#user_add input.born').val(obj[0].born).attr('disabled','true');};
						$('#user_add input.weight').focus();
					}else{
						setnull_for_obj($('#user_add'));
						$('#user_add input.id_user').focus();
						alert('Пользователь не найден!');
					};
				}
			});
		};
	});
	
	
	$('#sor_reg').tabs({ 
		active: 1,
		activate: function( event, ui ) {
			if(ui.newTab.index()==1){
				$('#user_add input.id_user').focus();
			};
		}	
	});
	
	$('#but_team_add').click(function() { 
	
		if($('select[name=id_fedok]').val()){
			$('#team_add').ajaxSubmit({ 
				beforeSubmit:  function(formData, jqForm, options){
					$('#sor_teams').hide();
					$('#loading_sor_add').show();
				},
				success:       function(responseText, statusText, xhr, $form){
					$('#sor_teams').show().find('tbody').html(responseText);
					$('#loading_sor_add').hide();
					init_button();
					$('#team_add select').val(0);
					$('#team_title').val('');
				}
			});
		}else{
			alert('Данные заполнены не верно!');
		};
		return false;
		
	});
	
	$('#team_add select').change(function(){
		var val = $(this).val();
		var title = $(this).children("option:selected").text();
		if(val>0){
			$('#team_title').val(title);
		};
	});
	
	$('#sors').on('click','.sor_event',function(e){
		var buttonset = $(this).children('.buttonset_sor').toggle();
	});


	function getSors(){
		//var date_start = $('#content_filter input[name=date_start]').val() + ' 23:59:59';
		//var date_finish = $('#content_filter input[name=date_finish]').val() + ' 23:59:59';

		var date_start = convert_date2sql($('#content_filter input[name=date_start]').val());
		var date_finish =convert_date2sql($('#content_filter input[name=date_finish]').val());
		
		var data = {
			status:		$('#content_filter div.buttonset div.active').attr('status'),
			date_start:	date_start,
			date_finish:date_finish,
			sport:		$('#content_filter select[name=sport]').val(),
			sport_type:	$('#content_filter select[name=sport_type]').val(),
			fedok:		$('#content_filter select[name=fedok]').val(),
			obl:		$('#content_filter select[name=obl]').val(),
			city:		$('#content_filter select[name=city]').val(),
			title:		$('#filter_by_title').val().trim()
		};
			
		$.ajax({  
			type: "POST",  
			url: "/ajax/ajax_sor.php?action=getSors",  
			data: data,
			beforeSend: function(){
				$('#sors').html("<img src='/images/loading.gif'>");
			},
			success: function(response){
				$('#sors').html(response);
				init_button();
			}
		});
	};
	
	// это странная функция скачанная с инета, для задержки ввода
	var delay = (function(){
		var timer = 0;
		return function (callback, ms){
			clearTimeout (timer);
			timer = setTimeout(callback, ms);
		};
	})();
	
	$('#content_filter').on('click', '.buttonset div.button', function(){
		if($(this).hasClass('active')){var flag = 1};
			$(this).parent('div.buttonset').find('div.button').removeClass('active');
		if(!flag){
			$(this).addClass('active');
		};
		getSors();
	});
	
	$('#content_filter input, #content_filter select').change(getSors);
	$('#filter_by_title').keyup(function(){
		if($(this).val().trim().length > 2){
			delay(function(){
				getSors();
			}, 300);
		};
	});
	
});