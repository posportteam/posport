<?php
header('Content-Type: text/html; charset=utf-8');
include '../config.php';
include '../libs_php/db_connect.php';

	$year 	= (int)$_POST['year'];
	$month 	= (int)$_POST['month'];
	
	if($_COOKIE['site_sport']){
		$usl_sport = " AND sports.id_sport = '$_COOKIE[site_sport]'";
	}elseif($_COOKIE['site_sport_type']){
		$usl_sport = " AND sports.id_sport_type = '$_COOKIE[site_sport_type]'";
	};
	
	$q_event = ("
		SELECT sors.title, geo_city.title, sports.title, YEAR(sors.date_start), MONTH(sors.date_start), DAY(sors.date_start), YEAR(sors.date_finish), MONTH(sors.date_finish), DAY(sors.date_finish)
		FROM sors, geo_city, sports
		WHERE 
			geo_city.id_city = sors.id_city 
			AND sors.id_sport = sports.id_sport
			AND (
				(YEAR(sors.date_start)='$year' && MONTH(sors.date_start)='$month')
				|| 
				(YEAR(sors.date_finish)='$year' && MONTH(sors.date_finish)='$month')
			)
			$usl_sport
	");
	$r_event = mysql_query($q_event);  
	while($event = mysql_fetch_array($r_event)){
		
		$year_st 	= $event[3];
		$month_st 	= $event[4];
		$day_st 	= $event[5];
		$year_fi 	= $event[6];
		$month_fi 	= $event[7];
		$day_fi 	= $event[8];
		
		// если соревнования закончились в этом месяце, считаем дни до его начала или до начала месяца
		if($month_fi==$month && $year_fi==$year){
			
			if($day_st<=$day_fi){
				// Соревнования начались в этом месяце
				$day_st_cur = $day_st;
			}else{
				// Соревнования начались в прошлом месяце
				$day_st_cur = 1;
			};
			while($day_st_cur<=$day_fi && 0<$day_fi){
				$events[$day_fi][] = array(	"title" => $event[0], "city" => $event[1], "sport" => $event[2]	);
				$day_fi--;
			};
		}else{
			// Если соревнования начались в этом месяце, но не закончились
			while($day_st<=31){
				$events[$day_st][] = array(	"title" => $event[0], "city" => $event[1], "sport" => $event[2]	);
				$day_st++;
			};
		};
	};

	if($events){
		foreach($events as $day => $events_in_day) {
			$response[] = array("day" => $day, "events" => $events_in_day);
		};
	};

print json_encode($response);
?>